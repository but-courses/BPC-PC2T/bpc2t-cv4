package cz.vutbr.feec;

import cz.vutbr.feec.entity.User;
import cz.vutbr.feec.service.BCryptUserRegistration;
import cz.vutbr.feec.service.PBKDF2UserRegistration;
import cz.vutbr.feec.iface.UserService;

/**
 * @author Pavel Seda
 */
public class App {

    public static void main(String[] args) {
        UserService userService1 = new BCryptUserRegistration();
        UserService userService2 = new PBKDF2UserRegistration();
        User user1 = new User(1L, "pavelseda@email.cz", "Pavel Seda", "mySuperSecretPassword".toCharArray());
        User user2 = new User(1L, "pavelseda@email.cz", "Pavel Seda", "mySuperSecretPassword".toCharArray());

        userService1.register(user1);
        userService2.register(user2);
    }
}
