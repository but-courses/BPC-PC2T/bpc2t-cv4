package cz.vutbr.feec.exceptions;

/**
 * My own exception.
 * 
 * @author Pavel Seda
 *
 */
public class ServiceLayerException extends RuntimeException {

	public ServiceLayerException() {
	}

	public ServiceLayerException(String message) {
		super(message);
	}

	public ServiceLayerException(String message, Throwable ex) {
		super(message, ex);
	}

	public ServiceLayerException(Throwable ex) {
		super(ex);
	}

	public ServiceLayerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
