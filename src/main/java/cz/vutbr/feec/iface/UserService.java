package cz.vutbr.feec.iface;

import cz.vutbr.feec.entity.User;

/**
 * Anotace @FunctionalInterface definuje, že jde o funkcionální rozhraní čili rozhraní, které má právě jednu abstraktní metodu.
 *
 * @author Pavel Seda
 */
@FunctionalInterface
public interface UserService {
    User register(User user);
}
