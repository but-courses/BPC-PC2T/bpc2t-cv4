package cz.vutbr.feec.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author Pavel Seda
 *
 */
public class ReadFile {

	/**
	 * Handling exceptions inside the method.
	 * 
	 * @param file the given File
	 * @return empty collection (advanced => modify the 'while' part to retrieve
	 *         List of Strings representing particular lines)
	 */
	public List<String> readFile(File file) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = br.readLine()) != null) {
				// go through the file
			}
		} catch (RuntimeException | IOException e) {
			// log or rethrow the exception
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// log or rethrow the exception
					e.printStackTrace();
				}
			}
		}
		return Collections.emptyList();
	}

	/**
	 * Not handling exceptions inside the method.
	 * 
	 * @param file the given File
	 * @return empty List of Strings containing file lines
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public List<String> readFileThrows(File file) throws RuntimeException, IOException {
		List<String> lines = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = "";
		while ((line = br.readLine()) != null) {
			lines.add(line);
		}
		// this is not efficient, if readLine throws exception than this resource will
		// never be closed..
		br.close();
		return lines;
	}

}
