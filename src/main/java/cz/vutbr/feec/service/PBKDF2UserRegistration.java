package cz.vutbr.feec.service;

import cz.vutbr.feec.entity.User;
import cz.vutbr.feec.iface.UserService;

/**
 * @author Pavel Seda
 */
public class PBKDF2UserRegistration implements UserService {

    @Override
    public User register(User user) {
        System.out.println("This registration is done using PBKDF2 algorithm..");
        return user;
    }
}
