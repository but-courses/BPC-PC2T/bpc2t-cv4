package cz.vutbr.feec.service;

/**
 * 
 * @author Pavel Seda
 *
 */
public class MathOperations {

	public static void main(String[] args) {
		int[] myInts = { 1, 2, 3, 4 };
		try {
			// java.lang.ArrayIndexOutOfBoundsException: 4
			System.out.println(myInts[4]);
		} catch (ArrayIndexOutOfBoundsException ex) {
			// handle this exception
		}

	}
}
