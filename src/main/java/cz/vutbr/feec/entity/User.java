package cz.vutbr.feec.entity;

import java.util.Arrays;

/**
 * @author Pavel Seda
 */
public class User {
    private Long id;
    private String email;
    private String name;
    private char[] password;

    public User(){}

    public User(Long id, String email, String name, char[] password) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password=" + Arrays.toString(password) +
                '}';
    }
}
