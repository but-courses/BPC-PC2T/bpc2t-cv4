package cz.vutbr.feec.entity;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Order {

	private long id;
	private String name;

	public Order(long id, String name) {
		super();
		setId(id);
		setName(name);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Name cannot be empty..");
		}
		this.name = name;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", name=" + name + "]";
	}

}
