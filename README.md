# Course: BPC2T - seminar 4
This project provides a solution for seminar 4. Includes topics like:
1. Class, getters/setters, constructors, main method creation
2. Try-catch block (or in advance try-with-resources block)
3. Creation of own exception classes
4. Distinguish between checked and unchecked exceptions
5. Identify basic exception classes

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```